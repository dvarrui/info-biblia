#INTRODUCCIÓN

Proyecto de documentación compartida mediante la herramienta de control
de versiones GIT. El repositorio está actualmente en bitbucket.

==========
CONTENIDOS
==========

Los distintos documentos se guardan en subdirectorios dentro del
direcotorio contents/. Los subdirectorios están para ayudar a organizar
los ficheros a medida que aumenten de número.

Por tanto, sólo hay que crear un fichero xml dentro de un subdirectorio
dentro de contents y escribir dentro de él.

FORMATO INTERNO

Principalmente los distintos documentos están escritos en formato XML
usando las etiquetas de DOCBOOK5. Es muy fácil de usar y además al ser
texto es fácil para GIT controlar los cambios.

MAKE

Para facilitar el trabajo usamos el make. Tiene varios targets y a continuación
pongo los más usados:
* make -> construye el pdf dentro del directorio output
* make install -> instala el software necesario de docbook5
* make html -> generar la salida en formato html dentro de output
* make pdf -> es lo mismo que make