#!/usr/bin/ruby
require 'rexml/document'

def init(filename='conf/config.xml')
	content=open(filename) { |f| f.read }
	
	begin
		@config=REXML::Document.new(content)
	rescue REXML::ParseException
		puts "Formato fichero <"+filename+"> incorrecto"
	end
end

def demo1
	puts @config.root.elements[1]
	puts @config.root.elements[1].class
	puts @config.root.elements[1].xpath
	puts @config.root.elements[1].attributes
	puts @config.root.elements[1].has_elements?
	puts @config.root.elements[1].elements[1]
	puts @config.root.elements[1].elements[1].xpath
	puts @config.root.elements[1].elements[1].attributes
	puts @config.root.elements[1].elements[1].has_elements?
	puts @config.root.elements[1].elements[1].has_text?
	puts @config.root.elements[1].elements[1].text
end

def show_config_list
	@config.root.elements.each do |i|
		if i.xpath=='/config/outputs'
			i.elements.each do |j|
				puts j.attributes['name']
			end
		end
	end
end

def get_global_value_for(op)
	@files=[]
	@config.root.elements.each do |i|
		if i.xpath=="/config/global"
			i.elements.each do |j|
				if j.name==op
					return j.text
				end
			end
		end
	end
end


def get_include_list_for(op='default')
	@files=[]
	@config.root.elements.each do |i|
		if i.xpath=='/config/outputs'
			dirbase=i.attributes['dirbase']
			i.elements.each do |j|
				if j.attributes['name']==op
					@title=j.attributes['title']
					j.elements.each do |k|
						@files << dirbase+k.attributes['src']+'.xml'
					end
				end
			end
		end
	end
end

def show_output
	system "cat conf/header1.xml"
	puts @title
	system "cat conf/header2.xml"

	puts "\n<!-- created automaticaly by create-main-file.rb script -->\n\n"
	@files.each do |f|
		puts '<xi:include href="'+f+'" />'      
	end
	puts "\n</book>\n\n"
end

init
get_include_list_for(ARGV[0]||'default')
show_output


