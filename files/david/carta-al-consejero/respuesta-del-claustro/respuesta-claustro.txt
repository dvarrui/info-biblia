
Puerto de la Cruz - Telesforo Bravo
Claustro de Profesores
Dirigido a: José Miguel Pérez , Consejero de Educación, Universidad y Sostenibilidad 
            y todo su Equipo



Texto:
 
Estimado Sr Consejero:
 
En respuesta al correo de envío masivo generado: 30/1/2013 8:11,
con asunto "Mensaje del Sr. Consejero", le enviamos el nuestro.

"Ahora en los 30 minutos que tengo para desayunar, aprovecho 
para responder a su correo masivo..."

==========================
RESULTADOS ULTIMA ENCUESTA

Gracias por la información sobre la EPA-2012.

Le agradecemos también que haga extensible a todos nosotros el 
agradecimiento y el logro por la reducción de la tasa de abandono escolar.
Como dice una compañera nuestra "Es de bien nacido, ser agradecido".
Nosotros también nos alegramos de que Canarias haya mejorado sus resultados.

Sin embargo no sería correcto aceptar un mérito que NO es nuestro.

=================
TRABAJO CONSTANTE

Sin embargo, estamos un poco desconcertados al respecto. Le explico.
Nosotros estamos haciendo la misma labor este curso, que el curso
pasado, y que el curso anterior, y será la misma que el próximo.

Por lo tanto entendemos que nuestro trabajo aunque necesario, NO
es un factor determinante en los resultados del índice que comenta.
De la misma forma que no participamos activamente en la mejora 
de dicho índice, tampoco somo partícipes cuando dicho índice empeora.
Nosotros hacemos siempre nuestro trabajo.

A pesar de todo, seguiremos, como no, comprometidos y colaborando
para ayudar en todo lo posible para cumplir de la mejor forma posible
los objetivos que marquen nuestros jefes desde la Consejería
de Educación, Universidad y Sostenibilidad.


=======================
ANÁLISIS DE SITUACIÓN 1

"Por fin he terminado mi turno de mañana, tengo 15 minutos hasta que
empiece mi turno de tarde. Como no tengo tiempo de almorzar entre
14:00 y 14:15... aprovecho para continuar mi escrito..."

Como siempre tratando de plantear propuestas y soluciones que ayuden 
a mejorar podríamos incluso adelantar una primera versión del  análisis 
de situación. Logicamente entendiendo que dicho análisis se escapa de nuestras 
competencias docentes, y que para nada queremos interferir con los 
expertos del tema.

¿Qué vemos nosotros que SI ha cambiado en nuestro trabajo diario?

* MAS HORAS: Trabajamos más horas en en centro y muchas más horas en casa, 
por lo que con frecuencia llegamos más cansados que otros años a trabajar.
* MAS RATIO:
* En aulgnas aulas no hay silla pa tanta gente.
* Los alumnos FP que terminan antes ayudan a explicar a los que van 
mas lentos porque el profesor no da a basto con todas las dudas del
grupo.
* AHORRO DE SALARIOS: En estos 10 años los funcionarios hemos perdido
un poder adquisitivo del 50%.

Todos esots cambios nos hacen reflexionar y llegar a la siguiente conclusión:
* Los profesores trabajan mejor con menos sueldo y más horas.
* Los alumnos se concentran más de pie, y aprenden mejor de sus compañeros
más que del profesor.

Por lo tanto la situción podría seguir evolucionando hacia:
* Menos profesores, más horas y por tanto más ahorro económico del ministerio.
Así mejoraremos los íncides de las encuentas antes mencionada.


================================
ANÁLISIS DE SITUACIÓN: FINLANDIA

Otra posible conclusión, los informes Pisa son un engaño de los paises
poderosos de la UE, para que los paises pobres sigan cometiendo errores.

Está claro que los parámetros que hacen de Finlandia un lider de Pisa
deben ser un error o un engaño puesto que aplicando medidas opuestas
se consigue mejores resultados:
* FInlandia: ration 12/15
* Finlandia: suledo de profesores más alto
* Finlandia: 15 min descanso entre clasew y clase de 45 minutos.

Está claro que en España somos un poco vagos como indican todos los estudios
del extranjero y debemos usar nuestro tiempo libre y de descanso para
mejorar la productividad. Los expertos españoles nos muestran ese camino.

=======================
ANÁLISIS DE SITUACIÓN - 3

Quizás otra visión, ya digo limitada desde nuestro modesto lugar.
Ya que no disponemos de toda la información de situación.

* Podría ser que los alumnos al no tener trabajo aprovechan para estudiar.
* El poco trabajo que hay como se lo llevan los que tienen título, los
  alumnos aprovechan para titular.
* Como algunos alumnos no tienen dinero para entretenerse por ahí
  vienen al centro (que es gratis) y se entretienen con sus amigos sin
  gastar dinero.
  
En cualquier caso, perdonen por atreverme a dar mi opinión sobre un tema
del que NO soy experto. Para seguir la tradición española no vamos a 
hablar de lo que no sabemos y vamos a dejar el análisis para los expertos
que son los que tienen toda la información. Mis disculpas por el
atrevimiento.

=============================
BUSCAR LA RESPUESTA AUTENTICA

"14:15 termino de trabajar y voy a buscar al niño al cole. Llego a casa
le doy la merienda y como tengo 15 minutos en lo que se calienta
el almuerzo voy a aprovechar para serguir escribirendo..."

Como doy por supuesto la existencia de un equipo de expertos que están
trabajando en la mejora de estos índices, no le quiero robar más tiempo.

Seguiremos trabajando según nos solicite y estamos a su disposición
para cualquier cometido que tenga a bien encomendarnos. Eso sí...
debo advertirle que sólo me quedan libres los sábados y domingos.
Y eso deberá usted mismo "lucharlo" con mi mujer que ya se los tenía
prometidos de cuando me casé.

Todavía me quedan las noches pero no le puedo garantizar que me
pueda mantener despierto el tiempo suficiente. Sé que es un fallo mío
y que no debería trasladarle pero no sé a quién trasladarlo. Soy
el último eslabón de la cadena docente y en casa tampoco mando mucho.


¡Muchas gracias por estar ahí, luchando por todos nosotros!

Un saludo

Quedamos al espera del siguiente correo masivo.


