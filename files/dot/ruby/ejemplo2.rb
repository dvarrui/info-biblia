#!/usr/bin/env ruby
require 'graph'

digraph do
  edge "C/C++", "Java"
  edge "Java", "Groovy"
  edge "Java", "Clojure"
  edge "Java", "JRuby"

  edge "C/C++", "Perl"
  edge "Perl", "PHP"
  edge "Perl", "Ruby"
  edge "Ruby", "Rubinius"
  edge "Ruby", "MacRuby"
  edge "Ruby", "JRuby"

  node_attribs << lightblue << filled
  %w{ Ruby JRuby MacRuby Rubinius }.each do|ruby|
    tomato << node(ruby)
  end

  save 'languages', 'png'
end

