#!/usr/bin/ruby

@dir=ARGV[0]||'.'
@filter=ARGV[1]||'*.*'
@counter=rand(1999)+7000
@target_name='nurenberg'
@target_ext='jpg'

puts "[INFO] Directory=#{@dir}"
puts "[INFO] Filter=#{@filter}"

all_files = Dir.entries(@dir+'/') - ['.', '..']

all_files.sort.each do |f|
	if f.index(".")!=0 then
		lsCommand="mv #{@dir}/#{f} #{@dir}/#{@target_name}_#{@counter}.#{@target_ext}"
		puts "[INFO] command: "+lsCommand
		system(lsCommand) 
		@counter+=1
	end
end
