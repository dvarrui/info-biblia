
=======================
Proceso de recuperación
4-FEB-2013
IES Puerto de la Cruz
=======================

Esto es un disco con W7 que había perdido (¿?) las particiones.
No había copia de seguridad.

Primero se clonó el disco con dd para no tocar el original sino la copia.
dd if=/dev/disco-original of=/dev/disco-clon

A continuación se montón el disco clonado en un PC con GNU/Linux Debian 6.
Y se hicieron las actuaciones que se ven en las capturas de pantalla.


-----------------------
David Vargas
Julio Ezequiel
Jose Acevedo
-----------------------
