

Un conocido tiene un problema con el disco. 
En principio era un disco de 500GB con una partición con SO W7... pero ya no está.
Usó un CDlIVE  GNU/Linux para acceder a los datos del disco pero
"Ha perdido las particiónes". No había copia de seguridad.

Primero se clonó el disco con dd para no tocar el original sino la copia.
dd if=/dev/disco-original of=/dev/disco-clon

A continuación se montón el disco clonado en un PC con GNU/Linux Debian 6.
Y se hicieron las actuaciones que se ven en las capturas de pantalla.

TESTDISK
TestDisk es una herramienta que podemos instalar en GNU/Linux, capaz de:
* Recuperar datos 
* Recuperar particiones perdidas
* Solucionar problemas del boot del sistema

A veces estos problemas están causados por: fallos de software, virus, 
errores humanos, etc.

Testdisk es fácil de usar. En nuestro caso nos interesa su capacidad para:
1 Fix partition table, recover deleted partition
2 Rebuild NTFS boot sector
3 Recover NTFS boot sector from its backup
4 Undelete files from FAT, exFAT, NTFS and ext2 filesystem
5 Copy files from deleted FAT, exFAT, NTFS and ext2/ext3/ext4 partitions. 


TestDisk has features for both novices and experts. For those who know little or nothing about data recovery techniques, TestDisk can be used to collect detailed information about a non-booting drive which can then be sent to a tech for further analysis. Those more familiar with such procedures should find TestDisk a handy tool in performing onsite recovery.




ENLACES DE INTERÉS
[1] TestDisk: http://www.cgsecurity.org/wiki/TestDisk


